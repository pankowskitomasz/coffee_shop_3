import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import TeasS1 from "../components/teas-s1";
import TeasS2 from "../components/teas-s2";
import TeasS3 from "../components/teas-s3";

class Teas extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <TeasS1/>
                <TeasS2/>
                <TeasS3/>
            </Container>    
        );
    }
}

export default Teas;